# satnogs-partchanger

These utilities modify SatNOGS Rotator parts to fit locally-available tubing materials.
It removes the need to be personally knowledgeable about using FreeCAD to modify the source files to change the relevant parts of the 3D models.

Input the desired diameter of your tubing in milimeters and you get new `.stl` and `.fcstd` files for the following parts:

* `axis_spacer`
* `axis_gear_flange`

## Git submodule dependency

This repository uses a `git submodule` to track the source files to be modified.
Be aware that the linked [satnogs-rotator](https://gitlab.com/wiredlab/satnogs-rotator) repository is rather large (~550 MiB) even though only a handful of `.fcstd` files are actually needed.
The advantage of this technique is to ensure the correct FreeCAD files are used when performing the customization.

When initially cloning, use:

    `git clone --recursive`

and/or, anytime afterwards use the following to keep the submodule updated after updating this repository:

    `git submodule update --init`

## Usage

These are built as standalone scripts which take a single argument as the desired diameter in millimeters.
For example:

    ./axis_homing.py 32.5

This will generate the files `axis_homing-32.5mm.fcstd` and `axis_homing-32.5mm.stl` in the `output/` directory.

According to FreeCAD docs importing FreeCAD as a python module is not recommended, but it is still unclear exactly the environment under which the scripts execute when run as `freecadcmd script.py` -- help in this area is appreciated!


## Limitations

The scripts check the given desired diameter to be within the allowable range and throw an informative exception.
Increasing the range (larger) of allowed inside diameters would require an audit of all the affected parts and ideally a prototype build to verify.


## Development notes

There are a few additional notes in the [docs](./docs/) folder of this repository.

### FreeCAD
Requires FreeCAD version **0.16 or later**.

Please see [FreeCAD Download](https://www.freecadweb.org/wiki/Download) page and use the Stable version.

NB: The Ubuntu 16.04 LTS archive uses an older FreeCAD version.
To get `0.16` on that platform, you will need to install from their PPA by following the instructions at (https://launchpad.net/~freecad-maintainers/+archive/ubuntu/freecad-stable).
You will also need to run `sudo apt install freecad` if FreeCAD wasn't already installed.


## License

&copy; 2017 [Dan White](https://github.com/etihwnad).

Licensed the same as the [SatNOGS](https://satnogs.org) project under the [AGPLv3](LICENSE).
