
import os
import sys


try:
    import FreeCAD
except ImportError:
    # TODO: get FreeCAD in the PYTHONPATH from outside of the script
    FREECADPATH = '/usr/lib/freecad/lib'
    print('INFO: Could not find FreeCAD in PYTHONPATH.')
    print('INFO: looking in hardcoded location: ' + FREECADPATH)
    sys.path.append(FREECADPATH)

    import FreeCAD


# if we get here, this should be available from FreeCAD's location
import Mesh


class Changer(object):
    def __init__(self, name):
        self.name = name
        FreeCAD.open('satnogs-rotator/rotator_parts/' + self.name + '.fcstd')
        self.doc = FreeCAD.ActiveDocument

    def validate_dimension(self, x, minmax):
        minimum, maximum = minmax
        if (x < minimum or x > maximum):
            raise ValueError('Dimension [%s] must be within range [%s, %s]' %
                             (x, str(minimum), str(maximum)))
        return x

    def saveFreecad(self):
        """Save the modified part as a new FreeCAD file."""
        # TODO: handle arbitrary path+names
        outname = os.getcwd() + os.path.sep + self.outname + '.fcstd'
        self.doc.saveAs(outname)
        print('Saved output to: ' + outname)

    def saveSTL(self):
        """Export the modified part as a STL file."""
        # Export an STL of the part
        objs = []
        objs.append(self.doc.getObject(self.stl_object))
        outname = self.outname + '.stl'
        Mesh.export(objs, outname)
        print('Saved output to: ' + outname)
